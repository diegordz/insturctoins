import React, { Component } from 'react';
import Budget from './budget.js';
import './App.css';


const budget = {
      "incomeStreams": [
        {
          "name": "Paycheck",
          "amount": "2000",
          "frequency": "2",
          
        },
        {
          "name": "Consulting",
          "amount": "2000",
          "frequency": "2",       
        },

      ],
      "expenses": [
        {
          "name": "Mortgage",
          "amount": "1300",
        },
        {
          "name": "Phone",
          "amount": "600",
        },
        {
          "name": "Internet",
          "amount": "120",
        },
      ]
    }

class App extends Component {
  render() {
    return (
      <div className="App">
        <Budget {...budget} />
      </div>
    );
  }
}

export default App;
